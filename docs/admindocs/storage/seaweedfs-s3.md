## Creating S3 user in seaweedfs volume

1. Login to seaweedfs master:

`kubectl exec -it -n seaweedfs $(kubectl get pods -n seaweedfs --selector=k8s-app=seaweed-master --output=jsonpath={.items..metadata.name}) sh`

1. Run weed shell:

`weed shell -filer seaweed-filer:8888 -master seaweed-master:9333`

1. Optionally inspect the current S3 config

`s3.configure`

1. Add the new config for the created Seaweedfs PVC

`s3.configure -access_key <key> -secret_key <key> -user <user> -buckets <bucket> -apply -actions Read,Write,List`

You have to manually generate the random keys. User is the name of the record (use the user's login).
