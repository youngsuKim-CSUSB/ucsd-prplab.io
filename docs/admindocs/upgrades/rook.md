## Upgrading rook

Follow the [most recent docs corresponding the the version being upgraded to](https://rook.io/docs/rook/v1.12/Upgrade/rook-upgrade/).

It's important to also use the [template for the 2nd cluster](https://github.com/rook/rook/blob/master/deploy/examples/common-second-cluster.yaml) for all additional ceph clusters (there are 5+ deployed). Remove all the PSP stuff from it.

```bash
cd deploy/examples
export ROOK_OPERATOR_NAMESPACE=rook-system
export ROOK_CLUSTER_NAMESPACE=rook
sed -i.bak \
    -e "s/\(.*\):.*# namespace:operator/\1: $ROOK_OPERATOR_NAMESPACE # namespace:operator/g" \
    -e "s/\(.*\):.*# namespace:cluster/\1: $ROOK_CLUSTER_NAMESPACE # namespace:cluster/g" \
  common.yaml

kubectl apply -f common.yaml -f crds.yaml
```

Delete all related to PSP from common-second-cluster.yaml

Fix "namespace: rook-ceph" -> "namespace: rook-system" in common-second-cluster.yaml

```
mkdir clusters

for i in rook-central rook-haosu rook-east rook-pacific rook-south-east; do NAMESPACE=$i envsubst < common-second-cluster.yaml > clusters/$i.yaml; done

cp common.yaml clusters/

kubectl diff -f clusters
```

see if changes make sense, then follow the guide:

```
kubectl apply -f clusters -f crds.yaml
```

... other commands from the guide

fix toolbox pods

## Upgrading ceph

**Before upgrading the major version, [make sure the OS and kernel support it](https://docs.ceph.com/en/latest/start/os-recommendations/)**
