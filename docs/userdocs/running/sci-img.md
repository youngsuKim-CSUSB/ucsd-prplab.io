We provide images based on [Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) project with NVIDIA CUDA libraries added, so that you could use those with GPUs. You can get the **list of software included** in the images on the [Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) web page.

All images are available in [JupyterHub](https://jupyterhub-west.nrp-nautilus.io), and also you can refer the images directly in your [Pod definition](/userdocs/running/jupyter/).

**The list of images and registry links are available in our [GitLab registry](https://gitlab.nrp-nautilus.io/prp/jupyter-stack/container_registry)**:

* Base: Notebook, Jupyter Lab and Jupyter Hub `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/base`
* Minimal: base + command-line tools `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/minimal`
* Desktop: GUI desktop `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/desktop`
* R: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/r`
* R Studio: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/r-studio`
* Julia: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/julia`
* SciPy: popular packages from the scientific Python ecosystem `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/scipy`
* Tensorflow: popular Python deep learning libraries `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/tensorflow`
* Relion Desktop: `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/relion-desktop`
* Datascience: includes libraries for data analysis from the Julia, Python, and R communities `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/datascience`
* PySpark: includes Python support for Apache Spark `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/pyspark`
* All Spark: includes Python and R support for Apache Spark `gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/all-spark`

The **NRP image** (gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/prp) is based on Stacks Tensorflow with addition of:

```
    astropy
    bowtie
    fastai
    keras
    nbgitpuller
    opencv-python
    psycopg2-binary
    tensorflow-probability
    torch
    torchvision
    visualdl
    git+https://github.com/veeresht/CommPy.git
```



We can add more libraries to this image on request in [Matrix](/userdocs/start/contact/).

The Desktop image has X Window system installed and you can launch the GUI interface in Jupyter with this image. It's based on Minimal stack one.

**The current stable tag for the images we (NRP) provide is `:v1.1`. Please use this tag if you want no changes in the image you're using, as `:latest` might change with upgrades.**

#### B-Data images

The [West JupyterHub](https://jupyterhub-west.nrp-nautilus.io) also provides access to the <a href="https://gitlab.b-data.ch/jupyterlab">B-Data images</a> These are available with and without CUDA support, for both x86 and ARM platforms. JupyterHub will automatically select the right one based on the request.
