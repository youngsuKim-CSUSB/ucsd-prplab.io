Guide: <https://kubevirt.io/2020/KubeVirt-installing_Microsoft_Windows_from_an_iso.html>

###### Running the ubuntu live vm

Here's the working example of an ubuntu VM with cloud-init, added SSH key and emptyDir scratch disk:

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: myvm
spec:
  running: false
  template:
    spec:
      accessCredentials:
      - sshPublicKey:
          propagationMethod:
            configDrive: {}
          source:
            secret:
              secretName: pub-keys
      architecture: amd64
      domain:
        cpu:
          cores: 8
        devices:
          autoattachGraphicsDevice: true
          autoattachSerialConsole: true
          disks:
          - disk:
              bus: virtio
            name: harddrive
          - disk:
              bus: virtio
            name: virtiocontainerdisk
            bootOrder: 1
          - disk:
              bus: virtio
            name: cloudinit
        machine:
          type: q35
        resources:
          limits:
            memory: 16Gi
          requests:
            memory: 16Gi
      volumes:
      - containerDisk:
          image: quay.io/containerdisks/ubuntu:22.04
        name: virtiocontainerdisk
      - emptyDisk:
          capacity: 16Gi
        name: harddrive
      - cloudInitConfigDrive:
          userData: |-
            #cloud-config
            disk_setup:
              /dev/vda:
                table_type: gpt
                layout: True
                overwrite: True
            fs_setup:
              - device: /dev/vda
                partition: 1
                filesystem: xfs
            mounts:
             - [ vda, /opt/data ]
        name: cloudinit
```

You also need to create the secret with your ssh key to login with:

```yaml
apiVersion: v1
data:
  key1: <base64-encoded key>
kind: Secret
metadata:
  name: pub-keys
type: Opaque
```

Use [virtctl](https://kubevirt.io/user-guide/operations/virtctl_client_tool/) to start the VM:

```bash
virtctl start myvm
```

Open VNC to it or ssh:

```bash
virtctl vnc myvm
virtctl ssh ubuntu@myvm
```