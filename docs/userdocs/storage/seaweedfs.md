[SeaweedFS](https://github.com/chrislusf/seaweedfs) is a high-performance distributed filesystem, optimized for working with huge number of files and also huge files.

**The current status is experimental, use at your own risk. Don't put there any data you don't want to lose, or data you can't easily share with others.**

### Seaweedfs filesystem total data use

<div id="observablehq-plot-2873ce04"></div>
<p>Credit: <a href="https://observablehq.com/d/2e49e5bb03a7f02e">Seaweedfs data use by Dmitry Mishin</a></p>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@observablehq/inspector@5/dist/inspector.css">
<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@5/dist/runtime.js";
import define from "https://api.observablehq.com/d/2e49e5bb03a7f02e.js?v=4";
new Runtime().module(define, name => {
  if (name === "plot") return new Inspector(document.querySelector("#observablehq-plot-2873ce04"));
});
</script>

### Currently available storageClasses:

<table>
  <thead>
    <tr class="header">
      <th>StorageClass</th>
      <th>Filesystem Type</th>
      <th>Region</th>
      <th>AccessModes</th>
      <th>Storage Type</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td markdown="span">seaweedfs-storage-hdd</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span">Spinning drives</td>
      <td markdown="span">24 TB</td>
    </tr>
    <tr>
      <td markdown="span">seaweedfs-storage-nvme</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span">NVME</td>
      <td markdown="span">80 TB</td>
    </tr>
  </tbody>
</table>

### Mounting

To test it, create a PersistentVolumeClaim with `seaweedfs-storage-hdd` storageClass:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: examplevol
spec:
  storageClassName: seaweedfs-storage-hdd
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: <volume size, f.e. 20Gi>
```

After you've created a PVC, you can see it's status (`kubectl get pvc pvc_name`). Once it has the Status `Bound`, you can attach it to your pod (claimName should match the name you gave your PVC):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: vol-pod
spec:
  containers:
  - name: vol-container
    image: ubuntu
    args: ["sleep", "36500000"]
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  restartPolicy: Never
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: examplevol
```

It's also [possible to access your volume via WebDAV](https://github.com/chrislusf/seaweedfs/wiki/WebDAV) from your local machine.

Seaweedfs also [supports S3 and HadoopFS](https://github.com/chrislusf/seaweedfs/wiki). You can request creating S3 credentials in [Matrix](/userdocs/start/contact/). To use S3 with SeaweedFS, use the following endpoint:

`https://swfs-s3.nrp-nautilus.io`

